import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromReducers from '../reducers';

export const getQuoteStore = createFeatureSelector('quote');

export const getQuoteEntities = createSelector(
  getQuoteStore,
  fromReducers.quote.quoteEntitySelectors.selectAll
);

export const getQuotes = createSelector(getQuoteEntities, entities => {
  return Object.values(entities);
});

export const getTopQuotes = createSelector(getQuoteEntities, entities => {
  return Object.values(entities).slice(1, 5);
});

export const getQuotesLoaded = createSelector(
  getQuoteStore,
  (quoteStore: fromReducers.quote.State) => quoteStore.loaded
);

export const getQuotesLoading = createSelector(
  getQuoteStore,
  (quoteStore: fromReducers.quote.State) => quoteStore.loading
);

export const getSelectedQuoteTickerHistoryValue = createSelector(
  getQuoteStore,
  (quoteStore: fromReducers.quote.State) => {
    if (quoteStore.selectedQuoteTickerHistory) {
      return quoteStore.selectedQuoteTickerHistory.map(data => data.value).reverse();
    }
    return null;
  }
);
export const getSelectedQuoteTickerHistoryDates = createSelector(
  getQuoteStore,
  (quoteStore: fromReducers.quote.State) => {
    if (quoteStore.selectedQuoteTickerHistory) {
      return quoteStore.selectedQuoteTickerHistory.map(data => data.date).reverse();
    }
    return null;
  }
);
export const getSelectedQuoteTickerInfo = createSelector(
  getQuoteStore,
  (quoteStore: fromReducers.quote.State) => {
    return quoteStore.selectedQuoteTickerInfo;
  }
);

export const getSearchQuotes = createSelector(
  getQuoteStore,
  (quoteStore: fromReducers.quote.State) => {
    return quoteStore.searchQuotes;
  }
);

export const getQuotesError = createSelector(
  getQuoteStore,
  (quoteStore: fromReducers.quote.State) => quoteStore.error
);
