import { Action } from '@ngrx/store';
import { EntityAdapter, createEntityAdapter, EntityState, Update } from '@ngrx/entity';
import { Quote } from 'app/models/quote';
import { QuoteActions, QuoteActionTypes } from '../actions/quote.actions';

export interface State extends EntityState<Quote> {
  loaded: boolean;
  loading: boolean;
  error: any;
  selectedQuoteTickerInfo: any[];
  selectedQuoteTickerHistory: any;
  searchQuotes: Quote[];
}

export const adapter: EntityAdapter<Quote> = createEntityAdapter<Quote>({
  selectId: (quote: Quote) => quote.ticker,
});

export const initialState: State = adapter.getInitialState({
  loaded: false,
  loading: false,
  selectedQuoteTickerInfo: null,
  selectedQuoteTickerHistory: null,
  error: null,
  searchTerm: '',
  searchQuotes: null
});

export function reducer(state = initialState, action: QuoteActions): State {
  switch (action.type) {
    case QuoteActionTypes.quoteGetQuotes:
    case QuoteActionTypes.quoteSearchQuotes:
    case QuoteActionTypes.quoteGetQuoteInfoByTicker:
    case QuoteActionTypes.quoteGetQuoteHistoryByTicker:
      return {
        ...state,
        loading: true
      };

    case QuoteActionTypes.quoteGetQuotesSuccess:
      return adapter.addAll(action.payload, {
        ...state,
        loading: false,
        loaded: true
      });

    case QuoteActionTypes.quoteGetQuoteInfoByTickerSuccess:
      return { ...state, selectedQuoteTickerInfo: action.payload, loading: false };

    case QuoteActionTypes.quoteGetQuoteHistoryByTickerSuccess:
      return { ...state, selectedQuoteTickerHistory: action.payload, loading: false };

    case QuoteActionTypes.quoteSearchQuotesSuccess:
      return {
        ...state,
        searchQuotes: action.payload,
        loading: false
      };

    case QuoteActionTypes.quoteSearchQuotesReset:
      return {
        ...state,
        searchQuotes: null
      };

    case QuoteActionTypes.quoteError:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.payload
      };

    default:
      return state;
  }
}

export const quoteEntitySelectors = adapter.getSelectors();
