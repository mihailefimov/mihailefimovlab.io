import * as quote from './quote.reducer';
import * as search from './search.reducer';

export { quote, search };
