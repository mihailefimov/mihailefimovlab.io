import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  QuoteActionTypes,
  GetQuotesSuccess,
  QuoteError,
  GetQuoteInfoByTicker,
  GetQuoteInfoByTickerSuccess,
  GetQuoteHistoryByTicker,
  GetQuoteHistoryByTickerSuccess,
  SearchQuotes,
  SearchQuotesSuccess
} from '../actions/quote.actions';
import { QuoteService } from 'app/services/quote.service';
import { switchMap, map, catchError, tap, debounceTime } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import * as fromRouterActions from '../actions/router.actions';

@Injectable()
export class QuoteEffects {
  constructor(private actions$: Actions, private quoteService: QuoteService) {}

  @Effect()
  loadQuotes$ = this.actions$.pipe(
    ofType(QuoteActionTypes.quoteGetQuotes),
    switchMap(() => this.quoteService.getQuotes()),
    map(quotes => new GetQuotesSuccess(quotes)),
    catchError(error => of(new QuoteError(error)))
  );

  @Effect()
  getQuoteInfoByTicker$ = this.actions$.pipe(
    ofType(QuoteActionTypes.quoteGetQuoteInfoByTicker),
    switchMap((action: GetQuoteInfoByTicker) =>
      this.quoteService.getQuoteInfo(action.payload)
    ),
    map(quote => {
      return new GetQuoteInfoByTickerSuccess(quote);
    }),
    catchError(error => of(new QuoteError(error)))
  );

  @Effect()
  getQuoteHistoryByTicker$ = this.actions$.pipe(
    ofType(QuoteActionTypes.quoteGetQuoteHistoryByTicker),
    switchMap((action: GetQuoteHistoryByTicker) =>
    this.quoteService.getQuoteHistory(action.payload)
    ),
    map(quote => new GetQuoteHistoryByTickerSuccess(quote)),
    catchError(error => of(new QuoteError(error)))
  );

  @Effect()
  searchQuotes$ = this.actions$.pipe(
    ofType(QuoteActionTypes.quoteSearchQuotes),
    switchMap((action: SearchQuotes) => {
      console.log('​QuoteEffects -> constructor -> ', action);
      return this.quoteService.searchQuotes(action.payload);
    }
    ),
    map(quotes => new SearchQuotesSuccess(quotes)),
    catchError(error => of(new QuoteError(error)))
  );

}
