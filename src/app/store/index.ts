import { Type } from '@angular/core';
import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { RouterReducerState, routerReducer } from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from 'environments/environment';
import * as fromReducers from './reducers';
import { QuoteEffects } from './effects/quote.effects';
import { RouterEffects } from './effects/router.effects';
import { SearchEffects } from './effects/search.effects';
import { RouterStateUrl } from './router';

export interface State {
  quote: fromReducers.quote.State;
  search: fromReducers.search.State;
  router: RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<State> = {
  quote: fromReducers.quote.reducer,
  search: fromReducers.search.reducer,
  router: routerReducer
};

export const effects = [QuoteEffects, SearchEffects, RouterEffects];
export const metaReducers: MetaReducer<State>[] = !environment.production ? [storeFreeze] : [];
