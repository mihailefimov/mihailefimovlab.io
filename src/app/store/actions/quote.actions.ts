import { Action } from '@ngrx/store';

import { Quote } from 'app/models/quote';

export enum QuoteActionTypes {
  quoteGetQuotes = '[Quote] Get',
  quoteGetQuotesSuccess = '[Quote] Get quotes success',
  quoteGetQuoteInfoByTicker = '[Quote] Get quote info by ticker',
  quoteGetQuoteInfoByTickerSuccess = '[Quote] Get quote info by ticker success',
  quoteGetQuoteHistoryByTicker = '[Quote] Get quote history by ticker',
  quoteGetQuoteHistoryByTickerSuccess = '[Quote] Get quote history by ticker success',
  quoteSearchQuotes = '[Quote] Search quotes',
  quoteSearchQuotesSuccess = '[Quote] Search quotes success',
  quoteSearchQuotesReset = '[Quote] Search quotes reset',
  quoteError = '[Quote] Error'
}

export class GetQuotes implements Action {
  readonly type = QuoteActionTypes.quoteGetQuotes;
}

export class GetQuotesSuccess implements Action {
  readonly type = QuoteActionTypes.quoteGetQuotesSuccess;
  constructor(public payload: Quote[]) {}
}

export class GetQuoteInfoByTicker implements Action {
  readonly type = QuoteActionTypes.quoteGetQuoteInfoByTicker;
  constructor(public payload: any) {}
}

export class GetQuoteInfoByTickerSuccess implements Action {
  readonly type = QuoteActionTypes.quoteGetQuoteInfoByTickerSuccess;
  constructor(public payload: any) {
  }
}
export class GetQuoteHistoryByTicker implements Action {
  readonly type = QuoteActionTypes.quoteGetQuoteHistoryByTicker;
  constructor(public payload: any) {
  }
}

export class GetQuoteHistoryByTickerSuccess implements Action {
  readonly type = QuoteActionTypes.quoteGetQuoteHistoryByTickerSuccess;
  constructor(public payload: any) {

  }
}

export class SearchQuotes implements Action {
  readonly type = QuoteActionTypes.quoteSearchQuotes;
  constructor(public payload: string) {}
}

export class SearchQuotesSuccess implements Action {
  readonly type = QuoteActionTypes.quoteSearchQuotesSuccess;
  constructor(public payload: Quote[]) {}
}

export class SearchQuotesReset implements Action {
  readonly type = QuoteActionTypes.quoteSearchQuotesReset;
}

export class QuoteError implements Action {
  readonly type = QuoteActionTypes.quoteError;
  constructor(public payload: any) {}
}

export type QuoteActions =
  | GetQuotes
  | GetQuotesSuccess
  | GetQuoteInfoByTicker
  | GetQuoteInfoByTickerSuccess
  | GetQuoteHistoryByTicker
  | GetQuoteHistoryByTickerSuccess
  | SearchQuotes
  | SearchQuotesSuccess
  | SearchQuotesReset
  | QuoteError;
