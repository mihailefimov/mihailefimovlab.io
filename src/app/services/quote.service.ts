import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Quote } from '../models/quote';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class QuoteService {

  private quotesUrl = 'https://api.intrinio.com';

  constructor(private http: HttpClient) { }

  getQuotes(): Observable<any> {
    const url = `${this.quotesUrl}/companies`;
    return this.http.get<any>(url)
      .pipe(
        map(response => response.data),
        catchError(this.handleError('getQuotes', []))
      );
  }

  getQuoteInfo(ticker: number): Observable<any> {
    const url = `${this.quotesUrl}/companies?identifier=${ticker}`;
    return this.http.get<any>(url, httpOptions).pipe(
      catchError(this.handleError<Quote>(`getQuoteInfo ticker=${ticker}`))
    );
  }

  getQuoteHistory(payload: any): Observable<any> {
    const httpParams: any = {
      'identifier': payload.ticker,
      'item': 'change',
    };

    if (payload.dateRange) {
      if (payload.dateRange.start) {
        httpParams.start_date = payload.dateRange.start;
      }
      if (payload.dateRange.end) {
        httpParams.end_date = payload.dateRange.end;
      }
    }

    const url = `${this.quotesUrl}/historical_data`;
    return this.http.get<any>(url, {...httpOptions, params: httpParams}).pipe(
      map(response => response.data),
      catchError(this.handleError<Quote>(`getQuoteHistory ticker=${payload.ticker}`))
    );
  }

  searchQuotes(term: string): Observable<any> {
    const url = `${this.quotesUrl}/companies?query=${term}`;
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<any>(url, httpOptions).pipe(
      map(response => response.data),
      catchError(this.handleError<any>('searchQuotes', []))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => of(result as T);
  }

}
