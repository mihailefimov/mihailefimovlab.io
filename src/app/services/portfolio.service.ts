import { Injectable } from '@angular/core';
import { AlertService } from 'ngx-alerts';

@Injectable()
export class PortfolioService {
  storeName = 'PORTOLIO_CONTAINER';

  constructor(private alertService: AlertService) {}

  getMyQuotes() {
    return JSON.parse(localStorage.getItem(this.storeName) || '');
  }

  setMyQuotes(quotes) {
    localStorage.setItem(this.storeName, JSON.stringify(quotes));
  }

  addToMyQuotes(newItemName, newItemValue) {
    const oldQuotes = JSON.parse(localStorage.getItem(this.storeName)) || {};
    oldQuotes[newItemName] = newItemValue;
    this.setMyQuotes(oldQuotes);
    this.alertService.success('New item addition success');
  }

  removeFromMyQuotes(itemName) {
    const oldQuotes = JSON.parse(localStorage.getItem(this.storeName));
    delete oldQuotes[itemName];
    this.setMyQuotes(oldQuotes);
    this.alertService.success('Remove item success');
  }


}
