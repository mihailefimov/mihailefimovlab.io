import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';

import { Quote } from 'app/models/quote';
import { QuoteService } from 'app/services/quote.service';

import * as fromSelectors from 'app/store/selectors';
import * as fromReducers from 'app/store/reducers';
import { GetQuotes } from 'app/store/actions/quote.actions';
import { PortfolioService } from 'app/services/portfolio.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {
  myQuotes = {};
  tickers = [];

  constructor(
    private store: Store<fromReducers.quote.State>,
    private portfolio: PortfolioService,
  ) {}

  ngOnInit() {
    this.updateList();
  }

  delete(ticker) {
    this.portfolio.removeFromMyQuotes(ticker);
    this.updateList();
  }

  updateList() {
    this.myQuotes = this.portfolio.getMyQuotes();
    this.tickers = Object.keys(this.myQuotes);
  }
}
