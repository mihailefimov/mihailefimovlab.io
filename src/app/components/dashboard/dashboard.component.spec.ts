import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import { PortfolioService } from 'app/services/portfolio.service';
import { DashboardComponent } from './dashboard.component';

describe('DashboardComponent', () => {
    let comp: DashboardComponent;
    let fixture: ComponentFixture<DashboardComponent>;

    beforeEach(() => {
        const storeStub = {};
        const portfolioServiceStub = {
            removeFromMyQuotes: () => ({}),
            getMyQuotes: () => ({})
        };
        TestBed.configureTestingModule({
            declarations: [ DashboardComponent ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: Store, useValue: storeStub },
                { provide: PortfolioService, useValue: portfolioServiceStub }
            ]
        });
        fixture = TestBed.createComponent(DashboardComponent);
        comp = fixture.componentInstance;
    });

    it('can load instance', () => {
        expect(comp).toBeTruthy();
    });

    it('tickers defaults to: []', () => {
        expect(comp.tickers).toEqual([]);
    });

    describe('ngOnInit', () => {
        it('makes expected calls', () => {
            spyOn(comp, 'updateList');
            comp.ngOnInit();
            expect(comp.updateList).toHaveBeenCalled();
        });
    });

});
