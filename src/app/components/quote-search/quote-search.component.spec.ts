import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { QuoteSearchComponent } from './quote-search.component';
import { MatAutocompleteModule } from '@angular/material';

describe('QuoteSearchComponent', () => {
    let comp: QuoteSearchComponent;
    let fixture: ComponentFixture<QuoteSearchComponent>;

    beforeEach(() => {
        const storeStub = {
            pipe: () => ({}),
            dispatch: () => ({})
        };
        const routerStub = {
            navigate: () => ({})
        };
        TestBed.configureTestingModule({
            imports: [
                MatAutocompleteModule,
            ],
            declarations: [ QuoteSearchComponent ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: Store, useValue: storeStub },
                { provide: Router, useValue: routerStub }
            ]
        });
        fixture = TestBed.createComponent(QuoteSearchComponent);
        comp = fixture.componentInstance;
    });

    it('can load instance', () => {
        expect(comp).toBeTruthy();
    });

    describe('ngOnInit', () => {
        it('makes expected calls', () => {
            const storeStub: Store<any> = fixture.debugElement.injector.get(Store);
            spyOn(storeStub, 'pipe');
            comp.ngOnInit();
            expect(storeStub.pipe).toHaveBeenCalled();
        });
    });

    describe('ngOnDestroy', () => {
        it('makes expected calls', () => {
            const storeStub: Store<any> = fixture.debugElement.injector.get(Store);
            spyOn(storeStub, 'dispatch');
            comp.ngOnDestroy();
            expect(storeStub.dispatch).toHaveBeenCalled();
        });
    });

});
