import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/observable/of';
import { Store, select } from '@ngrx/store';
import { debounceTime, distinctUntilChanged, switchMap, tap, first } from 'rxjs/operators';
import { Quote } from 'app/models/quote';
import * as fromSelectors from 'app/store/selectors';
import * as fromStore from 'app/store';
import { SearchReset, Search } from 'app/store/actions/search.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quote-search',
  templateUrl: './quote-search.component.html',
  styleUrls: ['./quote-search.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuoteSearchComponent implements OnInit, OnDestroy {
  searchTerm$: Observable<string>;
  quotes$: Observable<Quote[]>;

  constructor(
    private store: Store<fromStore.State>,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.quotes$ = this.store.pipe(select(fromSelectors.getSearchQuotes));
  }

  ngOnDestroy(): void {
    this.store.dispatch(new SearchReset());
  }

  search(term: string): void {
    if (term.length > 2) {
      this.store.dispatch(new Search(term));
    }
  }

  goTo(ticker: number): void {
    this.router.navigate(['/detail/' + ticker]);
  }
}
