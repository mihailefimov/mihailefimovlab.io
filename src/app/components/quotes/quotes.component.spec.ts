import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import { PortfolioService } from 'app/services/portfolio.service';
import { QuotesComponent } from './quotes.component';

describe('QuotesComponent', () => {
    let comp: QuotesComponent;
    let fixture: ComponentFixture<QuotesComponent>;

    beforeEach(() => {
        const storeStub = {
            dispatch: () => ({}),
            pipe: () => ({})
        };
        const portfolioServiceStub = {
            addToMyQuotes: () => ({})
        };
        TestBed.configureTestingModule({
            declarations: [ QuotesComponent ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: Store, useValue: storeStub },
                { provide: PortfolioService, useValue: portfolioServiceStub }
            ]
        });
        fixture = TestBed.createComponent(QuotesComponent);
        comp = fixture.componentInstance;
    });

    it('can load instance', () => {
        expect(comp).toBeTruthy();
    });

    describe('ngOnInit', () => {
        it('makes expected calls', () => {
            const storeStub: Store<any> = fixture.debugElement.injector.get(Store);
            spyOn(storeStub, 'dispatch');
            spyOn(storeStub, 'pipe');
            comp.ngOnInit();
            expect(storeStub.dispatch).toHaveBeenCalled();
            expect(storeStub.pipe).toHaveBeenCalled();
        });
    });

});
