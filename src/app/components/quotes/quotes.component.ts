import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Quote } from 'app/models/quote';
import * as fromReducer from 'app/store/reducers';
import * as fromSelectors from 'app/store/selectors';
import { GetQuotes } from 'app/store/actions/quote.actions';
import { PortfolioService } from 'app/services/portfolio.service';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuotesComponent implements OnInit {
  quotes$: Observable<Quote[]>;

  constructor(
    private store: Store<fromReducer.quote.State>,
    private portfolio: PortfolioService,

  ) {}

  ngOnInit() {
    this.store.dispatch(new GetQuotes());
    this.quotes$ = this.store.pipe(select(fromSelectors.getQuotes));
  }

  add(name, value): void {
    this.portfolio.addToMyQuotes(name, value);
  }
}
