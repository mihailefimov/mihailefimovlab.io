import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { PortfolioService } from 'app/services/portfolio.service';
import { QuoteDetailComponent } from './quote-detail.component';

describe('QuoteDetailComponent', () => {
    let comp: QuoteDetailComponent;
    let fixture: ComponentFixture<QuoteDetailComponent>;

    beforeEach(() => {
        const changeDetectorRefStub = {
            detectChanges: () => ({})
        };
        const activatedRouteStub = {
            params: {
                subscribe: () => ({})
            }
        };
        const storeStub = {
            pipe: () => ({})
        };
        const portfolioServiceStub = {
            addToMyQuotes: () => ({})
        };
        TestBed.configureTestingModule({
            declarations: [ QuoteDetailComponent ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: ChangeDetectorRef, useValue: changeDetectorRefStub },
                { provide: ActivatedRoute, useValue: activatedRouteStub },
                { provide: Store, useValue: storeStub },
                { provide: PortfolioService, useValue: portfolioServiceStub }
            ]
        });
        fixture = TestBed.createComponent(QuoteDetailComponent);
        comp = fixture.componentInstance;
    });

    it('can load instance', () => {
        expect(comp).toBeTruthy();
    });

    it('redraw defaults to: false', () => {
        expect(comp.redraw).toEqual(false);
    });

    describe('ngOnInit', () => {
        it('makes expected calls', () => {
            const storeStub: Store<any> = fixture.debugElement.injector.get(Store);
            spyOn(storeStub, 'pipe');
            comp.ngOnInit();
            expect(storeStub.pipe).toHaveBeenCalled();
        });
    });

});
