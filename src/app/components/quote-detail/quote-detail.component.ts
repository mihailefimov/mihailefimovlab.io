import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { map } from 'rxjs/operators';

import { Quote } from 'app/models/quote';
import { GetQuoteInfoByTicker, GetQuoteHistoryByTicker, GetQuotes } from 'app/store/actions/quote.actions';
import { Back } from 'app/store/actions/router.actions';
import * as fromReducer from 'app/store/reducers';
import * as fromSelectors from 'app/store/selectors';
import { Observable } from 'rxjs/Observable';
import { PortfolioService } from 'app/services/portfolio.service';

@Component({
  selector: 'app-quote-detail',
  templateUrl: './quote-detail.component.html',
  styleUrls: ['./quote-detail.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuoteDetailComponent implements OnInit {
  quoteInfo$: Observable<any>;
  quoteHistoryValue$: Observable<any>;
  quoteHistoryDates$: Observable<any>;
  ticker: string;
  dateRange = {
    start: '',
    end: '',
  };

  riskStatus = 'Risky';
  yieldValue = 0;

  redraw = false;

  public lineChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false
  };

  constructor(
    private store: Store<fromReducer.quote.State>,
    private activatedRoute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private portfolio: PortfolioService,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params['ticker']) {
        this.store.dispatch(new GetQuoteInfoByTicker(params['ticker']));
        this.store.dispatch(new GetQuoteHistoryByTicker({
          ticker: params['ticker']
        }));
        this.ticker = params['ticker'];
      }
    });
    this.quoteInfo$ = this.store.pipe(select(fromSelectors.getSelectedQuoteTickerInfo));
    this.quoteHistoryValue$ = this.store.pipe(
      select(fromSelectors.getSelectedQuoteTickerHistoryValue),
      map(value => {
        if (value && value.length > 10) {
          const last10Values = value.slice(-9, value.length);
          this.yieldValue = 365 * last10Values.reduce((a, b) => a + b, 0) / last10Values.length;
        } else if (value && value.length > 5) {
          const last5Values = value.slice(-4, value.length);
          if (Math.max.apply(null, last5Values) - Math.min.apply(null, last5Values) < 10) {
            this.riskStatus = 'Safe';
          }
        } else {
          this.riskStatus = 'Risky';
        }
        return value;
      })
    );
    this.quoteHistoryDates$ = this.store.pipe(select(fromSelectors.getSelectedQuoteTickerHistoryDates));
  }

  goBack(): void {
    this.store.dispatch(new Back());
  }

  updateHistory(): void {
    this.store.dispatch(new GetQuoteHistoryByTicker(
      {
        ticker: this.ticker,
        dateRange: this.dateRange,
      }
    ));
    this.redraw = true;
    this.cdRef.detectChanges();
    this.redraw = false;
  }

  addToPortfolio(ticker, name): void {
    this.portfolio.addToMyQuotes(ticker, name);
  }
}
